﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DoAnMHTTUD
{
    public class User
    {
        public string email;
        public string passphrase;
        public string name;
        public string dob;
        public string phone;
        public string address;

        public string salt;
        public string rsa_private_key;
        public string rsa_public_key;


        public User()
        {

        }

        public string ToXML()
        {
            User objectToSerialise = this;
            StringWriter Output = new StringWriter(new StringBuilder());
            XmlSerializer xs = new XmlSerializer(objectToSerialise.GetType());
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            xs.Serialize(Output, objectToSerialise, ns);
            return Output.ToString();
        }

        internal void FromXML(string xml)
        {
            byte[] byteArray = Encoding.Unicode.GetBytes(xml);
            MemoryStream stream = new MemoryStream(byteArray);

            XmlSerializer xs = new XmlSerializer(this.GetType());
            User t = (User)xs.Deserialize(stream);
            this.email = t.email;
            this.passphrase = t.passphrase;
            this.name = t.name;
            this.dob = t.dob;
            this.phone = t.phone;
            this.address = t.address;
            this.salt = t.salt;
            this.rsa_private_key = t.rsa_private_key;
            this.rsa_public_key = t.rsa_public_key;
        }
    }
}
