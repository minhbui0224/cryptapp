﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DoAnMHTTUD
{
    internal class Cryto
    {
        private static Random random = new Random();
        private static long position;

        internal static string GenSalt()
        {
            var saltBytes = new byte[32];
            using (var provider = new RNGCryptoServiceProvider())
                provider.GetNonZeroBytes(saltBytes);
            string Salt = Convert.ToBase64String(saltBytes);
            return Salt;
        }

        internal static string GenSaltedPassphrase(string salt, string passphrase)
        {
            string hash = ComputeHash(salt, passphrase);
            return hash;
        }

        internal static string[] GenRSAKeyPair(int v)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(v);
            
            return new string[] { RSA.ToXmlString(true), RSA.ToXmlString(false) };
        }

        static string ComputeHash(string salt, string password)
        {
            var saltBytes = Convert.FromBase64String(salt);
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 1000))
                return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));
        }

        public static bool Verify(string salt, string hash, string password)
        {
            return hash == ComputeHash(salt, password);
        }

        internal static string GenSaltedPassphrase()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        

        internal static void Encrypt(string input, string output, User receiver, string algo, string pad, string mode)
        {


            string salt = GenSalt();
            var saltBytes = Convert.FromBase64String(salt);

            SymmetricAlgorithm algorithm = getAlgofromString(algo);

            algorithm.BlockSize = algorithm.LegalBlockSizes[0].MaxSize;
            algorithm.KeySize = algorithm.LegalKeySizes[0].MaxSize;

            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes("DF", saltBytes, 10);
            
            algorithm.Key = key.GetBytes(algorithm.KeySize / 8);
            algorithm.IV = key.GetBytes(algorithm.BlockSize / 8);
            algorithm.Padding = getPadModefromString(pad);
            algorithm.Mode = getModeofOpfromString(mode);

            MyHeader header = GenHeader(algorithm, receiver);

            ICryptoTransform transform = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);
            using (FileStream destination = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                string headerxml = header.ToXML() + '\n';
                byte[] info = new UTF8Encoding(true).GetBytes(headerxml);
                destination.Write(info, 0, info.Length);

                using (CryptoStream cryptoStream = new CryptoStream(destination, transform, CryptoStreamMode.Write))
                {
                    using (FileStream source = new FileStream(input, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        source.CopyTo(cryptoStream);
                    }
                    if (!cryptoStream.HasFlushedFinalBlock)
                        cryptoStream.FlushFinalBlock();
                }
            }
            
        }

        private static CipherMode getModeofOpfromString(string mode)
        {
            mode = mode.ToUpper();
            if (mode.Contains("CBC"))
                return CipherMode.CBC;
            else if (mode.Contains("CFB"))
                return CipherMode.CFB;
            else if (mode.Contains("CTS"))
                return CipherMode.CTS;
            else if (mode.Contains("ECB"))
                return CipherMode.ECB;
            else if (mode.Contains("OFB"))
                return CipherMode.OFB;
            return CipherMode.CBC;
        }

        private static PaddingMode getPadModefromString(string pad)
        {
            pad = pad.ToUpper();
            if (pad.Contains("ANSIX923"))
                return PaddingMode.ANSIX923;
            else if (pad.Contains("ISO10126"))
                return PaddingMode.ISO10126;
            else if (pad.Contains("None"))
                return PaddingMode.None;
            else if (pad.Contains("Zeros"))
                return PaddingMode.Zeros;
            else if (pad.Contains("PKCS7"))
                return PaddingMode.PKCS7;
            return PaddingMode.ANSIX923; //default
        }

        private static SymmetricAlgorithm getAlgofromString(string algo)
        {
            SymmetricAlgorithm algorithm;
            algo = algo.ToLower();
            if (algo.Contains("aes"))
                algorithm = new AesCryptoServiceProvider();
            else
                algorithm = new DESCryptoServiceProvider();
            return algorithm;

        }

       

        private static MyHeader GenHeader(SymmetricAlgorithm algorithm, User receiver)
        {
            MyHeader s = new MyHeader();
            string pubkey = receiver.rsa_public_key;

            var csp = new RSACryptoServiceProvider();
            csp.FromXmlString(receiver.rsa_public_key);
            byte[] encryptedKey = csp.Encrypt(algorithm.Key, false);



            s.algo = algorithm.ToString();
            s.key = Convert.ToBase64String(encryptedKey);
            s.iv = Convert.ToBase64String(algorithm.IV);
            s.padding = algorithm.Padding.ToString();
            s.mode = algorithm.Mode.ToString();
            return s;
        }

        internal static void Decrypt(string input, string output, User receiver)
        {
            FileStream fin = new FileStream(input, FileMode.Open, FileAccess.Read, FileShare.Read);
                        fin.Seek(0, 0);

            MyHeader header = ParseHeaderFromFile(new StreamReader(fin),receiver);
            //fin.Seek(0, 0);

            //var reader = new StreamReader(fin);
            //var t = reader.Peek();

            //long t = fin.Position;

            SymmetricAlgorithm algorithm = getAlgoFromHeader(header, receiver);


            ICryptoTransform transform = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);

            MemoryStream s = new MemoryStream();

            using (FileStream destination = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                using (CryptoStream cryptoStream = new CryptoStream(destination, transform, CryptoStreamMode.Write))
                {
                    try
                    {
                        
                        fin.CopyTo(cryptoStream);
                        
                    }
                    catch (CryptographicException exception)
                    {
                        if (exception.Message == "Padding is invalid and cannot be removed.")
                            throw new ApplicationException("Universal Microsoft Cryptographic Exception (Not to be believed!)", exception);
                        else
                            throw;
                    }
                    
                }
            }
            
            fin.Close();

            string k = Encoding.UTF8.GetString(s.ToArray());

        }

        private static SymmetricAlgorithm getAlgoFromHeader(MyHeader header, User receiver)
        {
            SymmetricAlgorithm algorithm;

            algorithm = getAlgofromString(header.algo);

            algorithm.BlockSize = algorithm.LegalBlockSizes[0].MaxSize;
            algorithm.KeySize = algorithm.LegalKeySizes[0].MaxSize;

            var cp = new CspParameters();
            cp.KeyContainerName = "WhateverKeyContainerName";
            var csp = new RSACryptoServiceProvider(cp);

            csp.FromXmlString(receiver.rsa_private_key);

            byte[] encryptedSymKey = Convert.FromBase64String(header.key);
            byte[] decryptedKey = csp.Decrypt(encryptedSymKey, false);



            algorithm.Key = decryptedKey; //key.GetBytes(algorithm.KeySize / 8);
            algorithm.IV = Convert.FromBase64String(header.iv);
            algorithm.Padding = getPadModefromString(header.padding);
            algorithm.Mode = getModeofOpfromString(header.mode);

            return algorithm;
        }

        private static MyHeader ParseHeaderFromFile(StreamReader reader, User receiver)
        {
            int headersize = 0;

            var builder = new StringBuilder();
            string line="";
            while (true)
            {
                line = reader.ReadLine();
                builder.AppendLine(line);
                if (line == "</MyHeader>")
                {
                    position = reader.BaseStream.Position;
                    break;
                }
                        
            }

            

            string final = builder.ToString();

            headersize += final.Length -1;

            MyHeader header = new MyHeader();
            header.FromXML(final);

            reader.BaseStream.Seek(headersize, 0);
            return header;
        }

        internal static void SignDocument(string input, string output, User signer)
        {
            byte[] hashValue = computeSHA256(input);
            

            var csp = new RSACryptoServiceProvider();
            csp.FromXmlString(signer.rsa_private_key);

            byte[] signedHashValue = csp.SignData(hashValue, "SHA256");
            string signature = Convert.ToBase64String(signedHashValue);

            System.IO.File.WriteAllText(output, signature);
            
        }

        private static byte[] computeSHA256(string input)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            var filestream = new FileStream(input, FileMode.Open);
            filestream.Position = 0;
            byte[] hashValue = mySHA256.ComputeHash(filestream);
            filestream.Close();
            return hashValue;
        }

        internal static bool VerifySignature(string document, string signature, string rsa_public_key)
        {
            try
            {
                byte[] hashValue = computeSHA256(document);
                var csp = new RSACryptoServiceProvider();

                string signature_string = System.IO.File.ReadAllText(signature);
                byte[] byte_signature = Convert.FromBase64String(signature_string);

                csp.FromXmlString(rsa_public_key);
                bool success = csp.VerifyData(hashValue, "SHA256", byte_signature);
                return success;
            }
            catch (Exception)
            {

                return false;
            }
            
        }
    }
}