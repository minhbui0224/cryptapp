﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DoAnMHTTUD
{
    public class MyHeader
    {
        public string iv;
        public string key;
        public string padding;
        public string mode;
        public string algo;

        public MyHeader()
        {
        }

        public MyHeader(string iv, string key, string padding, string mode)
        {
            this.iv = iv;
            this.key = key;
            this.padding = padding;
            this.mode = mode;
        }
        public string ToXML()
        {
            MyHeader objectToSerialise = this;
            StringWriter Output = new StringWriter(new StringBuilder());
            XmlSerializer xs = new XmlSerializer(objectToSerialise.GetType());
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            xs.Serialize(Output, objectToSerialise, ns);
            return Output.ToString();
        }
        public void FromXML(string xml)
        {
            byte[] byteArray = Encoding.Unicode.GetBytes(xml);
            MemoryStream stream = new MemoryStream(byteArray);

            XmlSerializer xs = new XmlSerializer(this.GetType());
            MyHeader t = (MyHeader)xs.Deserialize(stream);
            this.algo = t.algo;
            this.iv = t.iv;
            this.key = t.key;
            this.padding = t.padding;
            this.mode = t.mode;
           

        }
    }
}
