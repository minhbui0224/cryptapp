﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace DoAnMHTTUD
{
    class UsersDB
    {
        private static Random random = new Random();
        public static UsersDB instance = new UsersDB();
        public OleDbConnection conn;
        public string szConnectionString;

        public UsersDB()
        {
                string szFilePath = "users.xlsx";
                this.szConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + szFilePath + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;MAXSCANROWS=0'";

        }

    public User GetUserByEmail(string email)
        {
            OleDbConnection conn = new OleDbConnection(szConnectionString);
            conn.Open();

            string query = "Select * from [Users$]  where email='" + email + "'";
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(query, conn);
            da.Fill(ds);
            var myData = ds.Tables[0].AsEnumerable().Select(row => new User
            {
                email = row.Field<string>(0),
                passphrase = row.Field<string>(1),
                name = row.Field<string>(2),
                dob = row.Field<string>(3),
                phone = row.Field<string>(4),
                address = row.Field<string>(5),
                salt = row.Field<string>(6),
                rsa_private_key = row.Field<string>(7),
                rsa_public_key = row.Field<string>(8)
            });
            conn.Close();

            List<User> list_mydata = myData.ToList();
            if (list_mydata.Count > 0)
                return list_mydata[0];
            else
                return null;
        }

         public User[] GetAllUser()
        {

            OleDbConnection conn = new OleDbConnection(szConnectionString);
            conn.Open();

            string query = "Select * from [Users$]";
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(query, conn);
            da.Fill(ds);
            var myData = ds.Tables[0].AsEnumerable().Select(row => new User
            {
                email = row.Field<string>(0),
                passphrase = row.Field<string>(1),
                name = row.Field<string>(2),
                dob = row.Field<string>(3),
                phone = row.Field<string>(4),
                address = row.Field<string>(5),
                salt = row.Field<string>(6),
                rsa_private_key = row.Field<string>(7),
                rsa_public_key = row.Field<string>(8)
            });
            conn.Close();

            List<User> list_mydata = myData.ToList();
            return list_mydata.ToArray();
        }

        public User UpdateUser(User u)
        {
            OleDbConnection conn = new OleDbConnection(szConnectionString);
            
            OleDbCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE [Users$] SET passphrase = @passphrase,name = @name,dob = @dob,phone = @phone,address = @address," +
                "salt = @salt,rsa_private_key = @rsa_private_key,rsa_public_key = @rsa_public_key"+
                " WHERE email = '"+ u.email + "'";
            command.Parameters.AddWithValue("@passphrase", u.passphrase);
            command.Parameters.AddWithValue("@name", u.name);
            command.Parameters.AddWithValue("@dob", u.dob);

            command.Parameters.AddWithValue("@phone", u.phone);
            command.Parameters.AddWithValue("@address", u.address);
            command.Parameters.AddWithValue("@salt", u.salt);
            command.Parameters.AddWithValue("@rsa_private_key", u.rsa_private_key);

            command.Parameters.AddWithValue("@rsa_public_key", u.rsa_public_key);


            conn.Open();
            command.ExecuteNonQuery();

            
            conn.Close();
            return u;
        }
        public User InsertUser(User u)
        {
            User check = this.GetUserByEmail(u.email);
            if (check != null)
                return null;

            OleDbConnection conn = new OleDbConnection(szConnectionString);

            OleDbCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO [Users$] VALUES(@email, @passphrase, @name, @dob, @phone, @address," +
                " @salt, @rsa_private_key, @rsa_public_key)";
            command.Parameters.AddWithValue("@email",  u.email);
            command.Parameters.AddWithValue("@passphrase", u.passphrase);
            command.Parameters.AddWithValue("@name", u.name);
            command.Parameters.AddWithValue("@dob", u.dob);

            command.Parameters.AddWithValue("@phone", u.phone);
            command.Parameters.AddWithValue("@address", u.address);
            command.Parameters.AddWithValue("@salt", u.salt);
            command.Parameters.AddWithValue("@rsa_private_key", u.rsa_private_key);

            command.Parameters.AddWithValue("@rsa_public_key", u.rsa_public_key);


            conn.Open();
            command.ExecuteNonQuery();


            conn.Close();
            return u;
        }
    }
}
