﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoAnMHTTUD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox4.Format += (s, e) => {
                e.Value = ((User)e.Value).email;
            };
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        User LoggedInUser = null;
        private string fileToBeEncrypted;
        private string fileToBeSign;
        private string fileToBeChecked;

        private void button1_Click(object sender, EventArgs e)
        {
            User u = UsersDB.instance.GetUserByEmail(textBox_email_login.Text);


            if (u != null && Cryto.Verify(u.salt, u.passphrase, textBox_passphrase_login.Text))
            {
                RenderUserInfo(u);
                button_updateinfo.Visible = true;
                button_create.Visible = false;
                label_loginstatus.Text = "Logged In:" + u.email;
                //MessageBox.Show("Success");
                textBox_email.ReadOnly = true;
                LoggedInUser = u;

                UpdateRSAGroupBox();
                UpdateEncryptionGroupBox();
            }
            else
            {
                MessageBox.Show("FAILED");
            }
            
            
        }

        private void UpdateEncryptionGroupBox()
        {
            
            comboBox4.DataSource = null;
            comboBox4.SelectedIndex = -1;
            

            User[] allUsers = UsersDB.instance.GetAllUser();
            comboBox4.DataSource = allUsers;
            
        }

        private void UpdateRSAGroupBox()
        {
            textBox_privateexport.Text = LoggedInUser.rsa_private_key;
            textBox_publicexport.Text = LoggedInUser.rsa_public_key;
        }

        private void RenderUserInfo(User u)
        {
            textBox_email.Text = u.email;

            textBox_name.Text = u.name;
            textBox_dob.Text = u.dob;
            textBox_phone.Text = u.phone;
            textBox_address.Text = u.address;
            textBox_passphrase.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            User u = new User
            {
                email = textBox_email.Text,
                
                name = textBox_name.Text,
                dob = textBox_dob.Text,
                phone = textBox_phone.Text,
                address = textBox_address.Text,
               
            };
            string passphrase = textBox_passphrase.Text;


            
            u.salt = Cryto.GenSalt();
            u.passphrase = Cryto.GenSaltedPassphrase(u.salt, passphrase);
            string[] rsa_keys = Cryto.GenRSAKeyPair(int.Parse(textBox_rsakeylength.Text));
            u.rsa_public_key = rsa_keys[1];
            u.rsa_private_key = rsa_keys[0];


            UsersDB.instance.InsertUser(u);

            MessageBox.Show("Success");
        }

        private void textBox_email_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_passphrase_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_updateinfo_Click(object sender, EventArgs e)
        {
            User u = LoggedInUser;

            u.email = textBox_email.Text;

            u.name = textBox_name.Text;
            u.dob = textBox_dob.Text;
            u.phone = textBox_phone.Text;
            u.address = textBox_address.Text;

            
            string passphrase = textBox_passphrase.Text;

            u.salt = Cryto.GenSalt();
            u.passphrase = Cryto.GenSaltedPassphrase(u.salt, passphrase);
            string[] rsa_keys = Cryto.GenRSAKeyPair(int.Parse(textBox_rsakeylength.Text));
            u.rsa_public_key = rsa_keys[1];
            u.rsa_private_key = rsa_keys[0];

            UsersDB.instance.UpdateUser(u);

            MessageBox.Show("Success");
        }

        private void button_genpassphrase_Click(object sender, EventArgs e)
        {
            textBox_passphrase.Text = Cryto.GenSaltedPassphrase();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            String xml = LoggedInUser.ToXML();
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.WriteAllText(dialog.FileName, xml);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                string xml;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    xml = System.IO.File.ReadAllText(dialog.FileName);
                    LoggedInUser.FromXML(xml);

                    UsersDB.instance.UpdateUser(LoggedInUser);
                    RenderUserInfo(LoggedInUser);
                    UpdateRSAGroupBox();

                }
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fileToBeEncrypted = dialog.FileName;
                    label14.Text = fileToBeEncrypted;
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        
        private void button5_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    User receiver = (User)comboBox4.SelectedItem;
                    Cryto.Encrypt(input: fileToBeEncrypted, output: dialog.FileName, receiver: receiver, algo:comboBox1.SelectedItem.ToString(), pad: comboBox3.SelectedItem.ToString(), mode: comboBox2.SelectedItem.ToString());
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fileToBeSign = dialog.FileName;
                    label24.Text = fileToBeSign;
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {

                    Cryto.Decrypt(input: fileToBeSign, output: dialog.FileName, receiver: LoggedInUser);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fileToBeSign = dialog.FileName;
                    label19.Text = fileToBeSign;
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string output = fileToBeSign + ".sig";
            Cryto.SignDocument(input: fileToBeSign, output: output, signer: LoggedInUser);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.FilterIndex = 2;
                dialog.RestoreDirectory = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fileToBeChecked = dialog.FileName;
                    label21.Text = fileToBeChecked;
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            User[] allUsers = UsersDB.instance.GetAllUser();
            foreach (User u in allUsers)
            {
                bool result = Cryto.VerifySignature(document: fileToBeChecked, signature: fileToBeChecked + ".sig", rsa_public_key: u.rsa_public_key);
                if (result == true)
                {
                    label26.Text ="verified:"+ u.email;
                    break;
                }
                else
                {
                    label26.Text = "Verification Failed";
                }
            }
            
        }

        private void textBox_phone_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
